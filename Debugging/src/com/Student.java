package com;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Shailesh Sonare
 */
public class Student {
    int rollno;

    public Student() {
    }

    public Student(int rollno) {
        this.rollno = rollno;
    }

    public int getRollno() {
        return rollno;
    }

    /**
     * set value of roll number field
     * @param rollno this will the the first parameter
     */
    public void setRollno(int rollno) {
        this.rollno = rollno;
    }
    
    
}
