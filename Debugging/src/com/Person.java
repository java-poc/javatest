/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com;

/**
 *
 * @author Shailesh Sonare
 */
public abstract class Person {
    int age;
    String mobile_no;

    public Person() {
    }
    
    public Person(int age, String mobile_no) {
        this.age = age;
        this.mobile_no = mobile_no;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    /**
     * this function just display the employee data
     */
    public void showData() {
        System.out.println(age + mobile_no);
    }
    
    public final void displayData() {
        System.out.println("akjdsklfjklsj");
    }
    
}
