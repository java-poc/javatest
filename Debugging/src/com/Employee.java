/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com;

/**
 * This class will manage the Employee related Data
 * @author Shailesh Sonare
 * whatever
 */
public class Employee extends Person {
    int emp_no;
    int salary;
    String name;

    
    /**
     * This will be parameterized constructor
     * @param emp_no var 1
     * @param salary var 2
     * @param name var 3
     */
    public Employee(int emp_no, int salary, String name) {
        super();
        this.emp_no = emp_no;
        this.salary = salary;
        this.name = name;
    }
    
    /**
     * this function just display the employee data
     */
    @Override
    public void showData() {
        System.out.println(emp_no + salary + name);
    }
}
