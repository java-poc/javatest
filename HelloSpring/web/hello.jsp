<%-- 
    Document   : hello
    Created on : 16 Mar, 2017, 12:32:53 PM
    Author     : Shailesh Sonare 
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.AbstractSet"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

    ArrayList list = new ArrayList();
    list.add("h");
    list.add("e");
    list.add("l");
    list.add("l");
    list.add("o");
    
    for(int i = 1; i < 10; i++) {
        list.add(i);
    }
    
    Iterator it = list.iterator();
    
    while(it.hasNext()) {
        out.println(it.next() + "<br/>");
    }
    

    HashSet hs = new HashSet();
    hs.add("A");
    hs.add("B");
    hs.add("C");
    hs.add("C");
    hs.add("C");
    
    HashMap hm = new HashMap();
    hm.put("a", "A");
    hm.put("b", "B");
    
    Iterator its = hs.iterator();
    
    while(its.hasNext()){
        out.println(its.next() + "<br/>");
    }
    
    Iterator itr = hm.entrySet().iterator();
    
    while(itr.hasNext()) {
        Map.Entry pair = (Map.Entry)itr.next();
        out.println(pair.getKey() + " => " + pair.getValue() + "<br/>");
    }

    String[] strarr = new String[10];
    
    for(int i = 0; i < 10; i++) {
        strarr[i] = "abc" + i;
    }
    
    for(String valu : strarr ) {
        out.println(valu);
    }

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
    </body>
</html>
