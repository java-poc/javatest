/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package equalsandhashcode;

import classes.Employee;

/**
 *
 * @author Shailesh Sonare
 */
public class EqualsAndHashCode {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Employee e1 = new Employee("Shailesh", 20, "DAVPS04995");
        Employee e2 = new Employee("Shailesh", 20, "DAVPS04995");
        
        System.out.println(e1.equals(e2));
        System.out.println("" + e1.getClass());
        System.out.println(e1 instanceof classes.Employee);
        System.out.println("" + e1.hashCode());
        System.out.println("" + e2.hashCode());
    }
    
}
