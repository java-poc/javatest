package multithreadingdemo;

class Hi extends Thread {

    @Override
    public void run() {
        try {
            for (int i = 0; i <= 10; i++) {
                Thread.sleep(2000);
                System.out.println(i + " Hi");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

class Hello extends Thread {

    @Override
    public void run() {
        try {
            for (int i = 0; i <= 10; i++) {
                Thread.sleep(2000);
                System.out.println(i + " Hello");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

class World extends Thread {

    @Override
    public void run() {
        try {
            for (int i = 0; i <= 10; i++) {
                Thread.sleep(2000);
                System.out.println(i + " World");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

class Nam implements Runnable {

    @Override
    public void run() {
        try {
            for (int i = 0; i <= 10; i++) {
                Thread.sleep(2000);
                System.out.println(i + " Nam");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}

public class MultithreadingDemo {

    public static void main(String[] args) {
        Hi hi = new Hi();
        Hello hello = new Hello();
        World world = new World();
        hi.start();
        hello.start();
        world.start();

	Runnable r2 = ()->{
		try{
			for(int i = 1; i <= 10; i++) {
				Thread.sleep(500);
				System.out.println("Lambda...");
			}
		} catch(Exception e) {
				System.out.println(e);
		}
	};
	new Thread(r2).start();
    }
}
