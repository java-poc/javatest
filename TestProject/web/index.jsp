<%-- 
    Document   : index
    Created on : 18 Apr, 2016, 5:46:45 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*" %>
<%@page import="java.io.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            int id = 0;
                String firstName = "";
                String lastName = "";
                Date dateOfBirth = null;
            try {
            String connectionURL = "jdbc:mysql://10.87.62.231/test";
                Connection connection = null; 
                Class.forName("com.mysql.jdbc.Driver").newInstance(); 
                connection = DriverManager.getConnection(connectionURL, "root", "");
                
                String query = "SELECT * FROM users";
 
                // create the java statement
                Statement st = connection.createStatement();

                // execute the query, and get a java resultset
                ResultSet rs = st.executeQuery(query);

                
                // iterate through the java resultset
                while (rs.next())
                {
                  id = rs.getInt("ID");
                  firstName = rs.getString("first_name");
                  lastName = rs.getString("last_name");
                  dateOfBirth = rs.getDate("dob");
                  // print the results  
                }
                System.out.format("%s, %s, %s, %s\n", id, firstName, lastName, dateOfBirth);
                st.close();
                
                if(!connection.isClosed())
                     out.println("Successfully connected to " + "MySQL server using TCP/IP...");
                connection.close();
            }catch(Exception ex){
                out.println("Unable to connect to database"+ex);
            }   

        %>
        <c:out value="${firstName}" escapeXml="false"/>
        <h1>Hello World!</h1>
    </body>
</html>
