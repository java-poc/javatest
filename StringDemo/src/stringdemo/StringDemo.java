/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stringdemo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Shailesh Sonare
 */
public class StringDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String s1 = "https://www.youtube.com/watch?v=WPvGqX-TXP0youtube";
        
        System.out.println(s1.substring(s1.indexOf(".") + 1, s1.lastIndexOf(".")));
        System.out.println(s1.replace("youtube", "vimeo"));
        
        String s2 = "helloworldofprogrammero";
        
        System.out.println(s2.replaceAll("(.*)o(.*)o(.*)", "$1#$2#$3"));
    }
    
}
