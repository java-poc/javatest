<%-- 
    Document   : test
    Created on : 14 Oct, 2016, 1:29:47 PM
    Author     : user
--%>


<%@page import="models.Test"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page import="org.hibernate.boot.registry.StandardServiceRegistryBuilder"%>
<%@page import="org.hibernate.cfg.Configuration"%>


<%
Configuration cfg = new Configuration();
cfg.configure("hibernate.cfg.xml");
StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cfg.getProperties());
SessionFactory factory = cfg.buildSessionFactory(ssrb.build());
Session sess = factory.openSession();

List bookmarks = sess.createQuery("FROM Test").list();


for (Iterator iterator = bookmarks.iterator(); iterator.hasNext();) {

    Test t = (Test) iterator.next();

    out.println(t.getFirstName());
    out.println(t.getLastName());
    

}

%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Bye World!</h1>
    </body>
</html>
