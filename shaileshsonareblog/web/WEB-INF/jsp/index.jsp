<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Welcome to Spring Web MVC project</title>
        <link href="<c:url value="/resources/css/default.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/js/default.css" />" rel="stylesheet">
    </head>

    <body>
        <img width="100px" height="50px" src="<c:url value="/resources/images/logo.gif"/>"/>
        <header></header>
        <aside></aside>
        <section>
            <form method="POST" action="<c:url value="">
                
            </form>
        </section>
        <footer>&copy; shaileshsonare.com</footer>
    </body>
</html>
