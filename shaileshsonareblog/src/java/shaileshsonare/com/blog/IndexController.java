package shaileshsonare.com.blog;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {
    @RequestMapping("/index")
    public ModelAndView welcomePage() {
        ModelAndView mv = new ModelAndView("index.jsp");
        return mv;
    }
}
