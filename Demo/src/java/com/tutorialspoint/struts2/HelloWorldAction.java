/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tutorialspoint.struts2;

/**
 *
 * @author user
 */
public class HelloWorldAction {

    private String name;
    private String lastname;

    public String execute() throws Exception {
        Employee emp = new Employee("Shailesh", "Sonare", 25000);
        ManageEmployee me = new ManageEmployee();
        System.out.println(me.insert(emp));
        return "failure";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    
}
