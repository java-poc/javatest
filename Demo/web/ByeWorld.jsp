<%-- 
    Document   : ByeWorld.jsp
    Created on : 19 May, 2016, 5:12:16 PM
    Author     : user
--%>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Bye World!</h1>
    <s:property value="name"/>
    <s:property value="lastname"/>
    </body>
</html>
