/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author Shailesh Sonare
 */
public class Student {
    String name;
    int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" + "name=" + name + ", age=" + age + '}';
    }
    
    
    public static void sortByName(ArrayList<Student> al) {
        
        Collections.sort(al, new Comparator<Student>(){
            @Override
            public int compare(Student o1, Student o2) {
                return o1.name.compareTo(o2.name);
            }
        });
        
    }
    
    public static void sortByNameDesc(ArrayList<Student> al) {
        
        Collections.sort(al, new Comparator<Student>(){
            @Override
            public int compare(Student o1, Student o2) {
                return o2.name.compareTo(o1.name);
            }
        });
        
    }
    
    public static void sortByAge(ArrayList<Student> al) {
        
        Collections.sort(al, new Comparator<Student>(){
            @Override
            public int compare(Student o1, Student o2) {
                if(o1.age > o2.age) {
                    return 1;
                } else if(o1.age < o2.age) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });
    }
    
    public static void sortByAgeDesc(ArrayList<Student> al) {
        
        Collections.sort(al, new Comparator<Student>(){
            @Override
            public int compare(Student o1, Student o2) {
                if(o1.age > o2.age) {
                    return -1;
                } else if(o1.age < o2.age) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
    }
}
