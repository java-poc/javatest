/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import com.google.gson.Gson;
import java.io.IOException;
import java.util.HashMap;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * REST Web Service
 *
 * @author Shailesh Sonare
 */
@Path("test")
public class Test {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of Test
     */
    public Test() {
    }

    /**
     * Retrieves representation of an instance of api.Test
     * @param id
     * @return an instance of java.lang.String
     * @throws java.io.IOException
     */
    @GET
    @Path("id/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(@PathParam("id") String id) throws IOException {
        //TODO return proper representation object
        //throw new UnsupportedOperationException();
        /**
         * HashMap hm = new HashMap();
        hm.put("key1", "value1");
        hm.put("key2", "value2");
        hm.put("key3", "value3");
        
        
        
        return gson.toJson(hm);
         */
        HashMap hm = new HashMap();
        hm.put("key1", "value1");
        hm.put("key2", "value2");
        hm.put("key3", "value3");
        hm.put("key4", id);
        ObjectMapper om = new ObjectMapper();
        return om.writeValueAsString(hm); //"{\"key1\":\"value1\",\"key2\":\"value2\",\"key3\":\"value3\"}";
    }

    /**
     * PUT method for updating or creating an instance of Test
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
