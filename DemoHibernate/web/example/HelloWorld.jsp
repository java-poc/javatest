<%@page import="example.Employees"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page import="org.hibernate.boot.registry.StandardServiceRegistryBuilder"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<html>
    <head>
        <title><s:text name="HelloWorld.message"/></title>
    </head>

    <body>
        <h2><s:property value="message"/></h2>

        <h3>Languages</h3>
        <ul>
            <li>
                <s:url id="url" action="HelloWorld">
                    <s:param name="request_locale">en</s:param>
                </s:url>
                <s:a href="%{url}">English</s:a>
            </li>

            <li>
                <s:url id="url" action="HelloWorld">
                    <s:param name="request_locale">es</s:param>
                </s:url>

                <s:a href="%{url}">Espanol</s:a>

            </li>
        </ul>
                <%
                    Configuration cfg = new Configuration();
                    cfg.configure("hibernate.cfg.xml");
                    StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cfg.getProperties());
//                    SessionFactory factory = cfg.buildSessionFactory(ssrb.build());
//                    Session sess = factory.openSession();
//                    out.println("asdfas");
//                    List employees = sess.createQuery("FROM Employees").list();
//
//                    for (Iterator iterator = employees.iterator(); iterator.hasNext();) {
//                        Employees e = (Employees) iterator.next();
//                        out.println(e.getFirstName());
//                        out.println(e.getLastName().concat("<br/>"));
//                    }
                %>
    </body>
</html>

