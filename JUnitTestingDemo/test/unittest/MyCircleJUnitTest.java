/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unittest;

import classes.MyCircle;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Shailesh Sonare
 */
public class MyCircleJUnitTest {
    
    public MyCircleJUnitTest() {
        System.out.println("I am from constructor");
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("I am from before class block");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("I am from after class block");
    }
    
    @Before
    public void setUp() {
        System.out.println("I am from before block");
    }
    
    @After
    public void tearDown() {
        System.out.println("I am from after block");
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void testArea() {
         MyCircle c = new MyCircle();
         c.setRadius(5);
         double area = 3.14 * 5 * 5;
         assertEquals(area, c.area(), 1.0);
     }
}
