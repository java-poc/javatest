package helloworld;

import java.util.Scanner;

class Student {
    String name;
    public Student(String name) {
        this.name = name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
}

public class HelloWorld {
    
    public static int add(int x, int y) {
        return x + y;
    }
    
    public static void foo(){
        System.out.println("hello fooo");
    }
    
    public static void main(String[] args) {
      
        Student s1 = new Student("Shailesh");
        
        Student s2 = s1;
        
        System.out.println("" + s1.getName());
        //s1.setName("Sonare");
        s1 = new Student("Sariputra");
        System.out.println("" + s1.getName());
        System.out.println("" + s2.getName());
        
//        Scanner s = new Scanner(System.in);
//        int name = s.nextInt();
//        System.out.println("Hello " + name * name);
        
        System.out.println("Addition = " + HelloWorld.add(5, 6));
        HelloWorld.foo();
        System.out.println("hello world");
        
        int x[] = new int[5];
        
        for (int i = 0; i < x.length; i++) {
            x[i] = i+1;
        }
        
        for (int i = 0; i < x.length; i++) {
            System.out.println(" " + x[i]);
        }
        System.out.println("test");
   
        
        for(int abc : x){
            System.out.println("abc : " + abc);
        }
        
    }
    
}
