/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Shailesh Sonare
 */
@Controller
@RequestMapping("home")
public class IndexController {
    @RequestMapping("index")
    public ModelAndView indexFun(){
        
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
          
        Employee s=(Employee)context.getBean("");
        s.show();
        
        return new ModelAndView("index");
    }
}
