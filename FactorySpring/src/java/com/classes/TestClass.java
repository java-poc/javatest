/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.classes;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.oxm.xmlbeans.XmlOptionsFactoryBean;
import org.springframework.web.context.support.XmlWebApplicationContext;

/**
 *
 * @author Shailesh Sonare
 */
public class TestClass {
    public TestClass() {
        System.out.println("I am from Test class");
        
        ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");        
        ComponentClass cc = (ComponentClass) context.getBean("compclass");

//        BeanFactory beanFactory = new DefaultListableBeanFactory();
//        XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader((BeanDefinitionRegistry) beanFactory);
//        reader.loadBeanDefinitions(new ClassPathResource("spring.xml"));        
//        ComponentClass cc = (ComponentClass) beanFactory.getBean("compclass");
        
        cc.testFun();
        
    }
}
