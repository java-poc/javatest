/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.classes;

/**
 *
 * @author Shailesh Sonare
 */
public class ComponentClass implements java.io.Serializable {
    
    private int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
    public ComponentClass() {
        System.out.println("I am from component class");
    }
    
    public ComponentClass(int age) {
        System.out.println("I am from component class");
    }
    
    public void testFun() {
        System.out.println("Test function run successfully : " + this.getAge());
    }
}
