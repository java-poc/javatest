/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.classes.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Index {
    @RequestMapping("/index")
    public ModelAndView welcomePage() {
        ModelAndView mv = new ModelAndView("index.jsp");
        return mv;
    }
}
