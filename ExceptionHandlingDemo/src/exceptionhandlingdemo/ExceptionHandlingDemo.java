package exceptionhandlingdemo;
import java.io.*;

class A {

	public void m() throws Exception {
		System.out.println("hello exception");
	}

}

public class ExceptionHandlingDemo {
    public static void main(String[] args) throws IOException, Exception {
	A a = new A();
	a.m();
	System.out.println("will run after");
    }

}
