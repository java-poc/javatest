/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservices;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author user
 */
@Path("service")
public class TimeService {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of TimeService
     */
    public TimeService() {
    }

    /**
     * Retrieves representation of an instance of webservices.TimeService
     * @return an instance of java.lang.String
     */
    @GET
    @Path("postmehod")
    @Produces(MediaType.TEXT_HTML)
    public String getHtml() {
        //TODO return proper representation object
        return "Hello I am from Web Service";
        //throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of TimeService
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.TEXT_HTML)
    public void putHtml(String content) {
    }
}
